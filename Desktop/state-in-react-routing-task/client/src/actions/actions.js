export const TOGGLE_SUBSCRIPTION = "TOGGLE_SUBSCRIPTION";
export const TOGGLE_SECTION_VISIBILITY = "TOGGLE_SECTION_VISIBILITY";
export const toggleSubscription = () => ({
  type: TOGGLE_SUBSCRIPTION,
});

export const toggleSectionVisibility = () => ({
  type: TOGGLE_SECTION_VISIBILITY,
});
