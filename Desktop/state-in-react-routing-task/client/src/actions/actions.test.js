import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import {
  TOGGLE_SUBSCRIPTION,
  TOGGLE_SECTION_VISIBILITY,
  toggleSubscription,
  toggleSectionVisibility,
} from "./actions";

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe("toggleSubscription action", () => {
  it("should create an action to toggle subscription", () => {
    const expectedAction = {
      type: TOGGLE_SUBSCRIPTION,
    };
    expect(toggleSubscription()).toEqual(expectedAction);
  });
});

describe("toggleSectionVisibility action", () => {
  it("should create an action to toggle section visibility", () => {
    const expectedAction = {
      type: TOGGLE_SECTION_VISIBILITY,
    };
    expect(toggleSectionVisibility()).toEqual(expectedAction);
  });
});

describe("async actions", () => {
  it("dispatches TOGGLE_SUBSCRIPTION after successful toggle", () => {
    const store = mockStore({});
    store.dispatch(toggleSubscription());

    const actions = store.getActions();
    expect(actions[0].type).toEqual(TOGGLE_SUBSCRIPTION);
  });

  it("dispatches TOGGLE_SECTION_VISIBILITY after successful toggle", () => {
    const store = mockStore({});
    store.dispatch(toggleSectionVisibility());
    const actions = store.getActions();
    expect(actions[0].type).toEqual(TOGGLE_SECTION_VISIBILITY);
  });
});
