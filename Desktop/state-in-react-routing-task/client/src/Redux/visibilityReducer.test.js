import visibilityReducer, { toggleVisibility } from "./visibilityReducer";

describe("visibilityReducer", () => {
  it("should toggle visibility when dispatched", () => {
    const initialState = {
      isVisible: true,
    };

    const nextState = visibilityReducer(initialState, toggleVisibility());
    expect(nextState.isVisible).toBe(false);

    const finalState = visibilityReducer(nextState, toggleVisibility());
    expect(finalState.isVisible).toBe(true);
  });

  it("should handle initial state", () => {
    const initialState = undefined;
    const nextState = visibilityReducer(initialState, {});
    expect(nextState.isVisible).toBe(true);
  });
});
