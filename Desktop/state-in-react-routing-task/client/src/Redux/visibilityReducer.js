import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  isVisible: true,
};

const visibilitySlice = createSlice({
  name: "visibility",
  initialState,
  reducers: {
    toggleVisibility: (state) => {
      state.isVisible = !state.isVisible;
    },
  },
});

export const { toggleVisibility } = visibilitySlice.actions;
export default visibilitySlice.reducer;
