import { configureStore } from "@reduxjs/toolkit";
import usersReducer from "../usersReducer.js";
import subscriptionReducer from "../subscribtionReducer.js";
import visibilityReducer from "../visibilityReducer.js";

const store = configureStore({
  reducer: {
    user: usersReducer,
    subscription: subscriptionReducer,
    visibility: visibilityReducer,
  },
});

export default store;
