import { configureStore } from "@reduxjs/toolkit";
import usersReducer from "../usersReducer.js";
import subscriptionReducer from "../subscribtionReducer.js";
import visibilityReducer from "../visibilityReducer.js";

describe("Redux Store Configuration", () => {
  it("should configure the store with the correct reducers and initial state", () => {
    // Arrange
    const expectedInitialState = {
      user: usersReducer(undefined, { type: "@@INIT" }),
      subscription: subscriptionReducer(undefined, { type: "@@INIT" }),
      visibility: visibilityReducer(undefined, { type: "@@INIT" }),
    };

    // Act
    const store = configureStore({
      reducer: {
        user: usersReducer,
        subscription: subscriptionReducer,
        visibility: visibilityReducer,
      },
    });

    // Assert
    expect(store.getState()).toEqual(expectedInitialState);
  });
});
