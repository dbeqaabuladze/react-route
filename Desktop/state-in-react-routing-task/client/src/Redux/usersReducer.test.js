import usersReducer, { setData } from "./usersReducer";

test("usersReducer should collect data correctly", () => {
  const initialState = { data: [] };
  const action = setData([{ id: 1, name: "John" }]);
  const newState = usersReducer(initialState, action);
  expect(newState.data).toEqual([{ id: 1, name: "John" }]);
});
