import configureStore from "redux-mock-store";
import thunk from "redux-thunk";
import axios from "axios";
import MockAdapter from "axios-mock-adapter";
import { subscribeUser } from "./subscribtionReducer.js";

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

describe("subscribeUser async action", () => {
  it("creates SUBSCRIBE_USER_FULFILLED when subscribing user is successful", async () => {
    const mockAxios = new MockAdapter(axios);
    const store = mockStore({});

    const expectedActions = [
      {
        type: "subscription/subscribeUser/pending",
        meta: {
          requestId: expect.any(String),
          requestStatus: "pending",
          arg: "test@example.com",
        },
      },
      {
        type: "subscription/subscribeUser/fulfilled",
        payload: {},
        meta: {
          requestId: expect.any(String),
          requestStatus: "fulfilled",
          arg: "test@example.com",
        },
      },
    ];

    mockAxios.onPost("http://localhost:3000/subscribe").reply(200, {});

    await store.dispatch(subscribeUser("test@example.com"));

    const dispatchedActions = store.getActions();

    expect(dispatchedActions).toEqual(expectedActions);
  });
});
