import { render, screen, fireEvent, waitFor } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { configureStore } from "@reduxjs/toolkit";
import JoinUsSection from "./joinOurProgramSection";
import { Provider } from "react-redux";
import store from "./Redux/reducers/rootReducer.js";
import usersReducer from "./Redux/usersReducer";
import subscriptionReducer from "./Redux/subscribtionReducer";
import visibilityReducer from "./Redux/visibilityReducer";

jest.mock("./email-validator.js", () => ({
  validateEmail: jest.fn(() => true),
}));
describe("JoinUsSection Component", () => {
  let store;

  beforeEach(() => {
    store = configureStore({
      reducer: {
        user: usersReducer,
        subscription: subscriptionReducer,
        visibility: visibilityReducer,
      },
    });
  });

  test("renders JoinUsSection", () => {
    render(
      <Provider store={store}>
        <JoinUsSection />
      </Provider>
    );

    const emailInput = screen.getByPlaceholderText(/Email/);
    fireEvent.change(emailInput, { target: { value: "test@example.com" } });

    const subscribeButton = screen.getByText("Subscribe");
    fireEvent.click(subscribeButton);
  });

  test("handles subscription correctly", async () => {
    const mockSubscribeUser = jest.fn();
    jest.mock("./Redux/thunk/subscribe", () => ({
      subscribeUser: mockSubscribeUser,
    }));

    render(
      <Provider store={store}>
        <JoinUsSection />
      </Provider>
    );

    const emailInput = screen.getByPlaceholderText("Email");
    fireEvent.change(emailInput, { target: { value: "test@example.com" } });

    const subscribeButton = screen.getByRole("button", { name: "Subscribe" });
    fireEvent.click(subscribeButton);

    await waitFor(() => {
      expect(mockSubscribeUser).toHaveBeenCalledWith("test@example.com");
    });
  });

  test("handles unsubscription correctly", async () => {
    const mockSubscribeUser = jest.fn();
    jest.mock("./Redux/thunk/subscribe", () => ({
      subscribeUser: mockSubscribeUser,
    }));

    render(
      <Provider store={store}>
        <JoinUsSection />
      </Provider>
    );

    const emailInput = screen.getByPlaceholderText("Email");
    fireEvent.change(emailInput, { target: { value: "test@example.com" } });

    const unsubscribeButton = screen.getByRole("button", {
      name: "Unsubscribe",
    });

    fireEvent.click(unsubscribeButton);

    await waitFor(() => {
      expect(mockSubscribeUser).toHaveBeenCalledWith({
        email: "test@example.com",
        isSubscribed: true, // Assuming your initial state is subscribed
      });
    });
  });
});
