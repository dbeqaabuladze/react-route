import React from "react";
import { render, screen, cleanup, waitFor } from "@testing-library/react";
import "@testing-library/jest-dom";
import userEvent from "@testing-library/user-event";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./Redux/reducers/rootReducer";
import App from "./App";
import JoinUsSection from "./joinOurProgramSection";
import CommunitySection from "./CommunitySection";
import UserDetailsPage from "./userDetails";
import NotFound from "./NotFound";

afterEach(cleanup);

describe("App Routes", () => {
  const renderWithRouter = (ui, { route = "/" } = {}) => {
    window.history.pushState({}, "Test page", route);
    return render(ui, { wrapper: Router });
  };

  test("renders JoinUsSection for / route", () => {
    renderWithRouter(
      <Provider store={store}>
        <Routes>
          <Route path="/" element={<JoinUsSection />} />
        </Routes>
      </Provider>
    );
    const joinUsText = screen.getByText(/Join Our Program/i);
    expect(joinUsText).toBeInTheDocument();
  });

  test("renders NotFound message", () => {
    renderWithRouter(
      <Provider store={store}>
        <Routes>
          <Route path="/*" element={<NotFound />} />
        </Routes>
      </Provider>,
      { route: "/non-existent-route" }
    );
    const notFoundText = screen.getByText(/Page not found/i);
    expect(notFoundText).toBeInTheDocument();
  });

  test("CommunitySection has button with hide-section classname", () => {
    renderWithRouter(
      <Provider store={store}>
        <Routes>
          <Route path="/community" element={<CommunitySection />} />
        </Routes>
      </Provider>,
      { route: "/community" }
    );
    const hideButton = screen.getByRole("button", { name: /hide section/i });
    expect(hideButton).toHaveClass("hide-section");
  });
});
