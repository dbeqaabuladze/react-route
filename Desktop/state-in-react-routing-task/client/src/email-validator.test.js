import { validateEmail, validEnding } from "./email-validator.js";

describe("validateEmail function", () => {
  it("returns true for valid email addresses", () => {
    const validEmails = [
      "example@gmail.com",
      "test@outlook.com",
      "user@yandex.ru",
    ];

    validEmails.forEach((email) => {
      expect(validateEmail(email)).toBe(true);
    });
  });

  it("returns false for invalid email addresses", () => {
    const invalidEmails = [
      "invalid@gmail.net",
      "test@outlook.net",
      "user@yahoo.com",
    ];

    invalidEmails.forEach((email) => {
      expect(validateEmail(email)).toBe(false);
    });
  });
});

describe("validEnding constant", () => {
  it("contains valid email endings", () => {
    const expectedValidEndings = ["gmail.com", "outlook.com", "yandex.ru"];

    expect(validEnding).toEqual(expect.arrayContaining(expectedValidEndings));
  });
});
