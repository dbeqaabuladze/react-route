import React, { useState, useEffect } from "react";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { toggleSubscription } from "./actions/actions.js";
import { validateEmail } from "./email-validator.js";
import { subscribeUser } from "./Redux/thunk/subscribe.js";
const JoinUsSection = () => {
  const dispatch = useDispatch();
  const isSubscribed = useSelector((state) => state.subscription.isSubscribed);

  const [email, setEmail] = useState(() => {
    const localEmail = localStorage.getItem("Email");
    return localEmail !== null ? localEmail : "";
  });

  const [isDisabled, setDisabled] = useState(false);
  const [opacity, setOpacity] = useState(1);
  const [displayMode, setDisplayMode] = useState("block");

  useEffect(() => {
    const buttonState = localStorage.getItem("Button state");
    if (buttonState) {
      dispatch(toggleSubscription());
    }
  }, [dispatch]);

  const handleToggleSubscription = async () => {
    if (!isDisabled && validateEmail) {
      disableBtn();
      changeButtonState();
      try {
        await dispatch(subscribeUser({ email, isSubscribed }));
        dispatch(toggleSubscription());
        setTimeout(enableBtn, 2000);
      } catch (error) {
        console.error("Error toggling subscription:", error);
      }
    } else if (!validateEmail) {
      console.error("Error subscribing:");
      throw new Error("Error subscribing: ");
    }
  };

  const handleSubscribe = async () => {
    if (!validateEmail(email)) {
      alert("Invalid email address");
      throw new Error("Invalid email address. Please try again.");
    }

    try {
      const response = await axios.post("http://localhost:3000/subscribe", {
        email: email,
      });

      if (response.status === 200) {
        console.log("Subscription successful!", response.data);
        alert("Subscription successful!");
        return response.data;
      } else {
        console.error("Error subscribing:", response.statusText);
        throw new Error("Error subscribing: " + response.statusText);
      }
    } catch (error) {
      console.error("Error subscribing:", error);
      throw new Error("Error subscribing: " + error.message);
    }
  };

  const unsubscribeEmail = async () => {
    try {
      const response = await axios.post("http://localhost:3000/unsubscribe", {
        email: email,
      });

      if (response.data.success) {
        return response.data;
      } else {
        throw new Error("Error unsubscribing: " + response.data.message);
      }
    } catch (error) {
      console.error("Error unsubscribing:", error);
      throw new Error("Error unsubscribing: " + error.message);
    }
  };

  const changeButtonState = () => {
    if (isSubscribed) {
      unsubscribeEmail();
    } else {
      handleSubscribe();
    }
  };

  const disableBtn = () => {
    setOpacity(0.5);
    setDisabled(true);
  };

  const enableBtn = () => {
    setOpacity(1);
    setDisabled(false);
  };

  return (
    <>
      <main id="app-container">
        <section
          className="app-section app-section--image-program"
          id="programContainer"
        >
          <h2 className="program-title">Join Our Program</h2>
          <h3 className="program-subtitle">
            Sed do eiusmod tempor incididunt
            <br />
            ut labore et dolore magna aliqua
          </h3>
          <form className="submitFieldWrapper" id="form">
            <div
              className="form-wrapper"
              id="emailForm"
              style={{
                display: displayMode,
              }}
            >
              <input
                className="form-input"
                id="submit-info"
                type="text"
                placeholder="Email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </div>
            <input
              id="subscribeButton"
              className="app-section__button submit-btn"
              type="button"
              value={isSubscribed ? "Unsubscribe" : "Subscribe"}
              aria-label={isSubscribed ? "Unsubscribe" : "Subscribe"}
              style={{
                opacity,
              }}
              onClick={handleToggleSubscription}
              disabled={isDisabled}
            />
          </form>
        </section>
      </main>
    </>
  );
};

export default JoinUsSection;
