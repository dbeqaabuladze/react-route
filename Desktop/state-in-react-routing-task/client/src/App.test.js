import React from "react";
import { render } from "@testing-library/react";
import { useDispatch } from "react-redux";
import App from "./App";
import { setData } from "./Redux/usersReducer";

jest.mock("react-redux", () => ({
  ...jest.requireActual("react-redux"),
  useDispatch: jest.fn(),
}));

jest.mock("./Redux/usersReducer", () => ({
  setData: jest.fn(),
}));

test("fetches and sets data using useEffect", async () => {
  useDispatch.mockReturnValue(jest.fn());
});
