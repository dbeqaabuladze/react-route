import React, { useEffect } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import UserDetailsPage from "./userDetails.js";
import "./index.css";
import JoinUsSection from "./joinOurProgramSection.js";
import CommunitySection from "./CommunitySection.js";
import NotFound from "./NotFound.js";
import { setData } from "./Redux/usersReducer.js";
import { useDispatch } from "react-redux";

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch("http://localhost:3000/community");
        if (!response.ok) throw new Error("There is problem");
        const jsonData = await response.json();

        dispatch(setData(jsonData));
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };
    fetchData();
  }, [dispatch]);

  return (
    <Routes>
      <Route path="/" element={<JoinUsSection />}></Route>
      <Route path="/community" element={<CommunitySection />}></Route>
      <Route path="/community/:id" element={<UserDetailsPage />} />
      <Route path="/*" element={<NotFound />}></Route>
    </Routes>
  );
}

export default App;
